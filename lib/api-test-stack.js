/* eslint-disable import/no-extraneous-dependencies */
const cdk = require( '@aws-cdk/core' );
const iam = require( '@aws-cdk/aws-iam' );
const apigateway = require( '@aws-cdk/aws-apigateway' );
const lambda = require( '@aws-cdk/aws-lambda' );
const lambdaNode = require( '@aws-cdk/aws-lambda-nodejs' );

const FUNCTION_RUNTIME = lambda.Runtime.NODEJS_14_X;

class ApiTestStack extends cdk.Stack {
	constructor( scope, id, props, env ) {
		super( scope, id, props );
		this.env = env;

		this.createApi();
		this.createLambdaFunction();
	}

	createApi() {
		this.restApi = new apigateway.RestApi( this, `${this.stackName}-rest-api`, {
			description: `${this.stackName}-rest-api`,
			restApiName: `${this.stackName}-rest-api`,
			apiKeySourceType: 'HEADER',
			defaultCorsPreflightOptions: {
				allowHeaders: [
					'Content-Type',
					'Authorization',
					'X-Amz-Date',
					'X-Api-Key',
					'x-api-key',
					'X-Amz-Security-Token',
					'X-Socket-Id',
					'X-Requested-With',
					'X-CSRF-TOKEN',
					'Account-Id',
					'account-id',
				],
				allowMethods: [ 'OPTIONS', 'GET', 'POST', 'PUT', 'PATCH', 'DELETE' ],
				allowCredentials: true,
				allowOrigins: this.env.variables.origins,
			},
			deployOptions: {
				stageName: this.env.name,
				tracingEnabled: true,
				dataTraceEnabled: true,
			},
		} );

		this.basePath = new apigateway.BasePathMapping(
			this,
			`${this.stackName}-base-path-mapping`,
			{
				domainName: apigateway.DomainName.fromDomainNameAttributes(
					this,
					'DomainName',
					{ domainName: `api.${this.env.name}.payground.com` },
				),
				restApi: this.restApi,
				stage: this.restApi.deploymentStage,
			},
		);
	}

	createRole() {
		if ( !this.role ) {
			this.role = new iam.Role( this, `${this.env.name}-test-api-lambda-role`, {
				assumedBy: new iam.ServicePrincipal( 'lambda.amazonaws.com' ),
				roleName: `${this.env.name}-test-api-lambda-role`,
				managedPolicies: [
					iam.ManagedPolicy.fromAwsManagedPolicyName(
						'service-role/AWSLambdaBasicExecutionRole',
					),
				],
			} );
			this.role.node.defaultChild.overrideLogicalId( 'GetSearchAPIRole' );
		}

		return this.role;
	}

	createLambdaFunction() {
		const environment = {
			runtimeEnvironment: this.env.name,
			NODE_OPTIONS: '--enable-source-maps',
		};

		const func = new lambdaNode.NodejsFunction(
			this,
			`${this.stackName}-test-function`,
			{
				bundling: {
					sourceMap: true,
					minify: true,
					externalModules: [ 'aws-sdk', 'aws-xray-sdk' ],
				},
				awsSdkConnectionReuse: true,
				entry: './src/index.js',
				handler: 'handler',
				functionName: `${this.stackName}-test-function`,
				runtime: FUNCTION_RUNTIME,
				code: lambda.Code.fromAsset( './src' ),
				timeout: cdk.Duration.seconds( 29 ),
				tracing: lambda.Tracing.ACTIVE,
				memorySize: 2048,
				environment,
				role: this.createRole(),
			},
		);

		const apiResource = this.restApi.root
			.addResource( 'v1' )
			.addResource( '{domain}' )
			.addResource( 'test' );
		const lambdaIntegration = new apigateway.LambdaIntegration( func, {
			timeout: cdk.Duration.seconds( 29 ),
			requestTemplates: { 'application/json': '{ "statusCode": "200"" }' },
		} );
		apiResource.addMethod( 'GET', lambdaIntegration, {
			apiKeyRequired: false,
			requestValidatorOptions: {
				validateRequestParameters: false,
				validateRequestBody: false,
				requestValidatorName: 'Validate Request Parameters & Headers',
			},
			requestParameters: {
				'method.request.header.Account-Id': true,
				'method.request.path.domain': true,
				'method.request.querystring.text': true,
				'method.request.querystring.from': false,
				'method.request.querystring.size': false,
				'method.request.querystring.specificTerms': false,
			},
		} );
	}
}

module.exports = { ApiTestStack };
