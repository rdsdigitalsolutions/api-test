module.exports = {
	env: {
		commonjs: true,
		es2021: true,
		node: true,
		jest: true,
	},
	extends: [
		'airbnb-base',
	],
	parserOptions: {
		ecmaVersion: 12,
	},
	rules: {
		'import/no-unresolved': 'off',
		indent: [
			'error',
			'tab',
			{
				SwitchCase: 1,
			},
		],
		'no-tabs': 'off',
		'no-multi-spaces': 'off',
		'space-in-parens': [
			'warn',
			'always',
		],
		'prefer-const': 'warn',
		'key-spacing': 'off',
		'new-cap': 'warn',
		'no-console': 'off',
		'computed-property-spacing': [
			'warn',
			'always',
		],
		'valid-typeof': 'warn',
		'no-param-reassign': 'warn',
		'array-bracket-spacing': [
			'error',
			'always',
		],
		'no-use-before-define': 'off',
		'no-throw-literal': 'off',
		'max-len': 'off',
		'padded-blocks': 'off',
	},
};
