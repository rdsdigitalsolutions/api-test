#!/usr/bin/env node

// eslint-disable-next-line import/no-extraneous-dependencies
const cdk = require( '@aws-cdk/core' );
const { ApiTestStack } = require( '../lib/api-test-stack' );

const app = new cdk.App();
const ENV_VARIABLES = {
	dev: {
		origins: [ '*' ],
		domain: 'api.dev.payground.com',
	},
};
// eslint-disable-next-line no-new
new ApiTestStack( app, `${process.env.DEPLOYMENT_ENV}-api-test-stack`, {
	/* If you don't specify 'env', this stack will be environment-agnostic.
	 * Account/Region-dependent features and context lookups will not work,
	 * but a single synthesized template can be deployed anywhere. */

	/* Uncomment the next line to specialize this stack for the AWS Account
	 * and Region that are implied by the current CLI configuration. */
	env: {
		account: process.env.CDK_DEFAULT_ACCOUNT,
		region: process.env.CDK_DEFAULT_REGION,
	},
	/* Uncomment the next line if you know exactly what Account and Region you
	 * want to deploy the stack to. */
	// env: { account: '123456789012', region: 'us-east-1' },

	/* For more information, see https://docs.aws.amazon.com/cdk/latest/guide/environments.html */
}, {
	name: process.env.DEPLOYMENT_ENV,
	variables: ENV_VARIABLES[ process.env.DEPLOYMENT_ENV ], // set env variables we can ...ENV_VARIABLEs and ...process.env to override if necessary
} );
